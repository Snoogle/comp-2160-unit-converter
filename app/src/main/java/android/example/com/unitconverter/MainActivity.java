package android.example.com.unitconverter;

import android.app.Activity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import org.w3c.dom.Text;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void convertValue(View view) {

        //Grab Value of the 'to' spinner
        Spinner mySpinner = (Spinner) findViewById(R.id.to_spinner);
        String text = mySpinner.getSelectedItem().toString();

        //Grab Value of the 'from' spinner
        Spinner mySpinner2 = (Spinner) findViewById(R.id.from_spinner);
        String text2 = mySpinner.getSelectedItem().toString();

        //Grab value of the input field
        EditText myInput = (EditText) findViewById(R.id.value_input);
        String content = myInput.getText().toString();
        double degree = Double.parseDouble(content);

        //Grab Result ID
        TextView txt = (TextView) findViewById(R.id.result_label);

        //Get result of text
        if(text == "Celsius"){
            double resultC = getCelsius(degree);
            txt.setText(String.valueOf(resultC));
        }
        if(text == "Select One"){
            txt.setText("Incorrect Type");
        }
        if(text == text2){
            txt.setText("Both types are equal");
        }else{
            double resultF = getfahrenheit(degree);
            txt.setText(String.valueOf(resultF));
        }
    }

    public double getfahrenheit(double value){
        return (value * 9/5) + 32;
    }

    public double getCelsius(double value){
        return (value - 32) * 5/9;
    }

}
